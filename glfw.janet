# glfw.janet
#
# GLFW bindings.
#
# TODO: More general .so path (https://github.com/janet-lang/janet/issues/1001).
(ffi/context "libglfw.so" :lazy true)

# Helpers.

(defn- from-c-array
  "Convert a C array into a Janet array."
  [c-array size ffi-type]
  (def type-size (ffi/size ffi-type))
  (def arr (array/new size))
  (for i 0 size
    (put arr i (ffi/read ffi-type c-array (* i type-size))))
  arr)

# Structs.

(def vidmode (ffi/struct
  :int   # width
  :int   # height
  :int   # redBits
  :int   # greenBits
  :int   # blueBits
  :int)) # refreshRate

# Values.

(def TRUE 1)
(def FALSE 0)
(def HAT_CENTERED 0)
(def HAT_UP 1)
(def HAT_RIGHT 2)
(def HAT_DOWN 4)
(def HAT_LEFT 8)
(def HAT_RIGHT_UP (bor HAT_RIGHT HAT_UP))
(def HAT_RIGHT_DOWN (bor HAT_RIGHT HAT_DOWN))
(def HAT_LEFT_UP (bor HAT_LEFT HAT_UP))
(def HAT_LEFT_DOWN (bor HAT_LEFT HAT_DOWN))
(def KEY_UNKNOWN -1)
(def KEY_SPACE 32)
(def KEY_APOSTROPHE 39)
(def KEY_COMMA 44)
(def KEY_MINUS 45)
(def KEY_PERIOD 46)
(def KEY_SLASH 47)
(def KEY_0 48)
(def KEY_1 49)
(def KEY_2 50)
(def KEY_3 51)
(def KEY_4 52)
(def KEY_5 53)
(def KEY_6 54)
(def KEY_7 55)
(def KEY_8 56)
(def KEY_9 57)
(def KEY_SEMICOLON 59)
(def KEY_EQUAL 61)
(def KEY_A 65)
(def KEY_B 66)
(def KEY_C 67)
(def KEY_D 68)
(def KEY_E 69)
(def KEY_F 70)
(def KEY_G 71)
(def KEY_H 72)
(def KEY_I 73)
(def KEY_J 74)
(def KEY_K 75)
(def KEY_L 76)
(def KEY_M 77)
(def KEY_N 78)
(def KEY_O 79)
(def KEY_P 80)
(def KEY_Q 81)
(def KEY_R 82)
(def KEY_S 83)
(def KEY_T 84)
(def KEY_U 85)
(def KEY_V 86)
(def KEY_W 87)
(def KEY_X 88)
(def KEY_Y 89)
(def KEY_Z 90)
(def KEY_LEFT_BRACKET 91)
(def KEY_BACKSLASH 92)
(def KEY_RIGHT_BRACKET 93)
(def KEY_GRAVE_ACCENT 96)
(def KEY_WORLD_1 161)
(def KEY_WORLD_2 162)
(def KEY_ESCAPE 256)
(def KEY_ENTER 257)
(def KEY_TAB 258)
(def KEY_BACKSPACE 259)
(def KEY_INSERT 260)
(def KEY_DELETE 261)
(def KEY_RIGHT 262)
(def KEY_LEFT 263)
(def KEY_DOWN 264)
(def KEY_UP 265)
(def KEY_PAGE_UP 266)
(def KEY_PAGE_DOWN 267)
(def KEY_HOME 268)
(def KEY_END 269)
(def KEY_CAPS_LOCK 280)
(def KEY_SCROLL_LOCK 281)
(def KEY_NUM_LOCK 282)
(def KEY_PRINT_SCREEN 283)
(def KEY_PAUSE 284)
(def KEY_F1 290)
(def KEY_F2 291)
(def KEY_F3 292)
(def KEY_F4 293)
(def KEY_F5 294)
(def KEY_F6 295)
(def KEY_F7 296)
(def KEY_F8 297)
(def KEY_F9 298)
(def KEY_F10 299)
(def KEY_F11 300)
(def KEY_F12 301)
(def KEY_F13 302)
(def KEY_F14 303)
(def KEY_F15 304)
(def KEY_F16 305)
(def KEY_F17 306)
(def KEY_F18 307)
(def KEY_F19 308)
(def KEY_F20 309)
(def KEY_F21 310)
(def KEY_F22 311)
(def KEY_F23 312)
(def KEY_F24 313)
(def KEY_F25 314)
(def KEY_KP_0 320)
(def KEY_KP_1 321)
(def KEY_KP_2 322)
(def KEY_KP_3 323)
(def KEY_KP_4 324)
(def KEY_KP_5 325)
(def KEY_KP_6 326)
(def KEY_KP_7 327)
(def KEY_KP_8 328)
(def KEY_KP_9 329)
(def KEY_KP_DECIMAL 330)
(def KEY_KP_DIVIDE 331)
(def KEY_KP_MULTIPLY 332)
(def KEY_KP_SUBTRACT 333)
(def KEY_KP_ADD 334)
(def KEY_KP_ENTER 335)
(def KEY_KP_EQUAL 336)
(def KEY_LEFT_SHIFT 340)
(def KEY_LEFT_CONTROL 341)
(def KEY_LEFT_ALT 342)
(def KEY_LEFT_SUPER 343)
(def KEY_RIGHT_SHIFT 344)
(def KEY_RIGHT_CONTROL 345)
(def KEY_RIGHT_ALT 346)
(def KEY_RIGHT_SUPER 347)
(def KEY_MENU 348)
(def KEY_LAST KEY_MENU)
(def MOD_SHIFT 0x0001)
(def MOD_CONTROL 0x0002)
(def MOD_ALT 0x0004)
(def MOD_SUPER 0x0008)
(def MOD_CAPS_LOCK 0x0010)
(def MOD_NUM_LOCK 0x0020)
(def MOUSE_BUTTON_1 0)
(def MOUSE_BUTTON_2 1)
(def MOUSE_BUTTON_3 2)
(def MOUSE_BUTTON_4 3)
(def MOUSE_BUTTON_5 4)
(def MOUSE_BUTTON_6 5)
(def MOUSE_BUTTON_7 6)
(def MOUSE_BUTTON_8 7)
(def MOUSE_BUTTON_LAST MOUSE_BUTTON_8)
(def MOUSE_BUTTON_LEFT MOUSE_BUTTON_1)
(def MOUSE_BUTTON_RIGHT MOUSE_BUTTON_2)
(def MOUSE_BUTTON_MIDDLE MOUSE_BUTTON_3)
(def JOYSTICK_1 0)
(def JOYSTICK_2 1)
(def JOYSTICK_3 2)
(def JOYSTICK_4 3)
(def JOYSTICK_5 4)
(def JOYSTICK_6 5)
(def JOYSTICK_7 6)
(def JOYSTICK_8 7)
(def JOYSTICK_9 8)
(def JOYSTICK_10 9)
(def JOYSTICK_11 10)
(def JOYSTICK_12 11)
(def JOYSTICK_13 12)
(def JOYSTICK_14 13)
(def JOYSTICK_15 14)
(def JOYSTICK_16 15)
(def JOYSTICK_LAST JOYSTICK_16)
(def GAMEPAD_BUTTON_A 0)
(def GAMEPAD_BUTTON_B 1)
(def GAMEPAD_BUTTON_X 2)
(def GAMEPAD_BUTTON_Y 3)
(def GAMEPAD_BUTTON_LEFT_BUMPER 4)
(def GAMEPAD_BUTTON_RIGHT_BUMPER 5)
(def GAMEPAD_BUTTON_BACK 6)
(def GAMEPAD_BUTTON_START 7)
(def GAMEPAD_BUTTON_GUIDE 8)
(def GAMEPAD_BUTTON_LEFT_THUMB 9)
(def GAMEPAD_BUTTON_RIGHT_THUMB 10)
(def GAMEPAD_BUTTON_DPAD_UP 11)
(def GAMEPAD_BUTTON_DPAD_RIGHT 12)
(def GAMEPAD_BUTTON_DPAD_DOWN 13)
(def GAMEPAD_BUTTON_DPAD_LEFT 14)
(def GAMEPAD_BUTTON_LAST GAMEPAD_BUTTON_DPAD_LEFT)
(def GAMEPAD_BUTTON_CROSS GAMEPAD_BUTTON_A)
(def GAMEPAD_BUTTON_CIRCLE GAMEPAD_BUTTON_B)
(def GAMEPAD_BUTTON_SQUARE GAMEPAD_BUTTON_X)
(def GAMEPAD_BUTTON_TRIANGLE GAMEPAD_BUTTON_Y)
(def GAMEPAD_AXIS_LEFT_X 0)
(def GAMEPAD_AXIS_LEFT_Y 1)
(def GAMEPAD_AXIS_RIGHT_X 2)
(def GAMEPAD_AXIS_RIGHT_Y 3)
(def GAMEPAD_AXIS_LEFT_TRIGGER 4)
(def GAMEPAD_AXIS_RIGHT_TRIGGER 5)
(def GAMEPAD_AXIS_LAST GAMEPAD_AXIS_RIGHT_TRIGGER)
(def NO_ERROR 0)
(def NOT_INITIALIZED 0x00010001)
(def NO_CURRENT_CONTEXT 0x00010002)
(def INVALID_ENUM 0x00010003)
(def INVALID_VALUE 0x00010004)
(def OUT_OF_MEMORY 0x00010005)
(def API_UNAVAILABLE 0x00010006)
(def VERSION_UNAVAILABLE 0x00010007)
(def PLATFORM_ERROR 0x00010008)
(def FORMAT_UNAVAILABLE 0x00010009)
(def NO_WINDOW_CONTEXT 0x0001000A)
(def FOCUSED 0x00020001)
(def ICONIFIED 0x00020002)
(def RESIZABLE 0x00020003)
(def VISIBLE 0x00020004)
(def DECORATED 0x00020005)
(def AUTO_ICONIFY 0x00020006)
(def FLOATING 0x00020007)
(def MAXIMIZED 0x00020008)
(def CENTER_CURSOR 0x00020009)
(def TRANSPARENT_FRAMEBUFFER 0x0002000A)
(def HOVERED 0x0002000B)
(def FOCUS_ON_SHOW 0x0002000C)
(def RED_BITS 0x00021001)
(def GREEN_BITS 0x00021002)
(def BLUE_BITS 0x00021003)
(def ALPHA_BITS 0x00021004)
(def DEPTH_BITS 0x00021005)
(def STENCIL_BITS 0x00021006)
(def ACCUM_RED_BITS 0x00021007)
(def ACCUM_GREEN_BITS 0x00021008)
(def ACCUM_BLUE_BITS 0x00021009)
(def ACCUM_ALPHA_BITS 0x0002100A)
(def AUX_BUFFERS 0x0002100B)
(def STEREO 0x0002100C)
(def SAMPLES 0x0002100D)
(def SRGB_CAPABLE 0x0002100E)
(def REFRESH_RATE 0x0002100F)
(def DOUBLEBUFFER 0x00021010)
(def CLIENT_API 0x00022001)
(def CONTEXT_VERSION_MAJOR 0x00022002)
(def CONTEXT_VERSION_MINOR 0x00022003)
(def CONTEXT_REVISION 0x00022004)
(def CONTEXT_ROBUSTNESS 0x00022005)
(def OPENGL_FORWARD_COMPAT 0x00022006)
(def OPENGL_DEBUG_CONTEXT 0x00022007)
(def OPENGL_PROFILE 0x00022008)
(def CONTEXT_RELEASE_BEHAVIOR 0x00022009)
(def CONTEXT_NO_ERROR 0x0002200A)
(def CONTEXT_CREATION_API 0x0002200B)
(def SCALE_TO_MONITOR 0x0002200C)
(def COCOA_RETINA_FRAMEBUFFER 0x00023001)
(def COCOA_FRAME_NAME 0x00023002)
(def COCOA_GRAPHICS_SWITCHING 0x00023003)
(def X11_CLASS_NAME 0x00024001)
(def X11_INSTANCE_NAME 0x00024002)
(def NO_API 0)
(def OPENGL_API 0x00030001)
(def OPENGL_ES_API 0x00030002)
(def NO_ROBUSTNESS 0)
(def NO_RESET_NOTIFICATION 0x00031001)
(def LOSE_CONTEXT_ON_RESET 0x00031002)
(def OPENGL_ANY_PROFILE 0)
(def OPENGL_CORE_PROFILE 0x00032001)
(def OPENGL_COMPAT_PROFILE 0x00032002)
(def CURSOR 0x00033001)
(def STICKY_KEYS 0x00033002)
(def STICKY_MOUSE_BUTTONS 0x00033003)
(def LOCK_KEY_MODS 0x00033004)
(def RAW_MOUSE_MOTION 0x00033005)
(def CURSOR_NORMAL 0x00034001)
(def CURSOR_HIDDEN 0x00034002)
(def CURSOR_DISABLED 0x00034003)
(def ANY_RELEASE_BEHAVIOR 0)
(def RELEASE_BEHAVIOR_FLUSH 0x00035001)
(def RELEASE_BEHAVIOR_NONE 0x00035002)
(def NATIVE_CONTEXT_API 0x00036001)
(def EGL_CONTEXT_API 0x00036002)
(def OSMESA_CONTEXT_API 0x00036003)
(def ARROW_CURSOR 0x00036001)
(def IBEAM_CURSOR 0x00036002)
(def CROSSHAIR_CURSOR 0x00036003)
(def HAND_CURSOR 0x00036004)
(def HRESIZE_CURSOR 0x00036005)
(def VRESIZE_CURSOR 0x00036006)
(def CONNECTED 0x00040001)
(def DISCONNECTED 0x00040002)
(def JOYSTICK_HAT_BUTTONS 0x00050001)
(def COCOA_CHDIR_RESOURCES 0x00051001)
(def COCOA_MENUBAR 0x00051002)
(def DONT_CARE -1)
(def VERSION_MAJOR 3)
(def VERSION_MINOR 3)
(def VERSION_REVISION 7)
(def RELEASE 0)
(def PRESS 1)
(def REPEAT 2)

# Bindings.

(ffi/defbind glfwInit :bool [])

(ffi/defbind glfwTerminate :void [])

(ffi/defbind glfwInitHint :void
  [hint :int value :int])

(ffi/defbind glfwGetVersion :void
  [major :ptr minor :ptr rev :ptr])

(ffi/defbind glfwGetVersionString :string [])

(ffi/defbind glfwGetError :int
  [description :ptr])

(ffi/defbind glfwGetMonitors :ptr
  [count :ptr])

(ffi/defbind glfwGetPrimaryMonitor :ptr [])

(ffi/defbind glfwGetMonitorPos :void
  [monitor :ptr xpos :ptr ypos :ptr])

(ffi/defbind glfwGetMonitorWorkarea :void
  [monitor :ptr xpos :ptr ypos :ptr width :ptr height :ptr])

(ffi/defbind glfwGetVideoModes :ptr
  [monitor :ptr count :ptr])

(ffi/defbind glfwGetVideoMode :ptr
  [monitor :ptr])

(ffi/defbind glfwWindowHint :void
  [hint :int value :int])

(ffi/defbind glfwCreateWindow :ptr
  [width :int height :int title :string monitor :ptr share :ptr])

(ffi/defbind glfwDestroyWindow :void
  [window :ptr])

(ffi/defbind glfwWindowShouldClose :bool
  [window :ptr])

(ffi/defbind glfwGetWindowPos :void
  [window :ptr xpos :ptr ypos :ptr])

(ffi/defbind glfwGetWindowSize :void
  [window :ptr width :ptr height :ptr])

(ffi/defbind glfwGetFramebufferSize :void
  [window :ptr width :ptr height :ptr])

(ffi/defbind glfwRestoreWindow :void
  [window :ptr])

(ffi/defbind glfwMaximizeWindow :void
  [window :ptr])

(ffi/defbind glfwShowWindow :void
  [window :ptr])

(ffi/defbind glfwHideWindow :void
  [window :ptr])

(ffi/defbind glfwFocusWindow :void
  [window :ptr])

(ffi/defbind glfwRequestWindowAttention :void
  [window :ptr])

(ffi/defbind glfwPollEvents :void [])

(ffi/defbind glfwWaitEvents :void [])

(ffi/defbind glfwWaitEventsTimeout :void
  [timeout :double])

(ffi/defbind glfwGetInputMode :int
  [window :ptr mode :int])

(ffi/defbind glfwSetInputMode :void
  [window :ptr mode :int value :int])

(ffi/defbind glfwGetKey :int
  [window :ptr key :int])

(ffi/defbind glfwGetMouseButton :int
  [window :ptr button :int])

(ffi/defbind glfwGetCursorPos :void
  [monitor :ptr xpos :ptr ypos :ptr])

(ffi/defbind glfwSetCursorPos :void
  [monitor :ptr xpos :double ypos :double])

(ffi/defbind glfwGetTime :double [])

(ffi/defbind glfwSetTime :void
  [time :double])

(ffi/defbind glfwGetTimerValue :uint64 [])

(ffi/defbind glfwGetTimerFrequency :uint64 [])

(ffi/defbind glfwMakeContextCurrent :void
  [window :ptr])

(ffi/defbind glfwGetCurrentContext :ptr [])

(ffi/defbind glfwSwapBuffers :void
  [window :ptr])

(ffi/defbind glfwVulkanSupported :int [])

# More idiomatic bindings.

(def init glfwInit)
(def terminate glfwTerminate)
(def init-hint glfwInitHint)
(def get-version-string glfwGetVersionString)
(def get-primary-monitor glfwGetPrimaryMonitor)
(def window-hint glfwWindowHint)
(def create-window glfwCreateWindow)
(def destroy-window glfwDestroyWindow)
(def window-should-close glfwWindowShouldClose)
(def restore-window glfwRestoreWindow)
(def maximize-window glfwMaximizeWindow)
(def show-window glfwShowWindow)
(def hide-window glfwHideWindow)
(def focus-window glfwFocusWindow)
(def request-window-attention glfwRequestWindowAttention)
(def poll-events glfwPollEvents)
(def wait-events glfwWaitEvents)
(def wait-events-timeout glfwWaitEventsTimeout)
(def get-input-mode glfwGetInputMode)
(def set-input-mode glfwSetInputMode)
(def get-key glfwGetKey)
(def get-mouse-button glfwGetMouseButton)
(def set-cursor-pos glfwSetCursorPos)
(def get-time glfwGetTime)
(def set-time glfwSetTime)
(def get-timer-value glfwGetTimerValue)
(def get-timer-frequency glfwGetTimerFrequency)
(def get-current-context glfwGetCurrentContext)
(def make-context-current glfwMakeContextCurrent)
(def swap-buffers glfwSwapBuffers)
(def vulkan-supported glfwVulkanSupported)

(defn get-version
  "Return an array @[major minor revision]."
  []
  (def major (ffi/write :int 0))
  (def minor (ffi/write :int 0))
  (def rev (ffi/write :int 0))
  (glfwGetVersion major minor rev)
  @[(ffi/read :int major) (ffi/read :int minor) (ffi/read :int rev)])

(defn get-error
  "Return an array @[error-code description]."
  []
  (def description (ffi/write :ptr nil))
  (def error-code (glfwGetError description))
  @[error-code description])

(defn get-monitors
  "Return an array of GLFWMonitor pointers."
  []
  (def number-ptr (ffi/write :int 0))
  (def monitors (glfwGetMonitors number-ptr))
  (def number (ffi/read :int number-ptr))
  (if (= nil monitors)
    @[]
    (from-c-array monitors number :ptr)))

(defn get-monitor-pos
  "Returns an array @[xpos ypos]."
  [monitor]
  (def xpos-ptr (ffi/write :int 0))
  (def ypos-ptr (ffi/write :int 0))
  (glfwGetMonitorPos monitor xpos-ptr ypos-ptr)
  (def xpos (ffi/read :int xpos-ptr))
  (def ypos (ffi/read :int ypos-ptr))
  @[xpos ypos])

(defn get-monitor-workarea
  "Returns an array @[xpos ypos width height]."
  [monitor]
  (def xpos-ptr (ffi/write :int 0))
  (def ypos-ptr (ffi/write :int 0))
  (def width-ptr (ffi/write :int 0))
  (def height-ptr (ffi/write :int 0))
  (glfwGetMonitorWorkarea monitor xpos-ptr ypos-ptr width-ptr height-ptr)
  (def xpos (ffi/read :int xpos-ptr))
  (def ypos (ffi/read :int ypos-ptr))
  (def width (ffi/read :int width-ptr))
  (def height (ffi/read :int height-ptr))
  @[xpos ypos width height])

(defn get-video-modes
  "Returns an array of vidmode."
  [monitor]
  (def count-ptr (ffi/write :int 0))
  (def video-modes (glfwGetVideoModes monitor count-ptr))
  (def num-modes (ffi/read :int count-ptr))
  (from-c-array video-modes num-modes vidmode))

(defn get-video-mode
  "Returns a vidmode."
  [monitor]
  (def mode-ptr (glfwGetVideoMode monitor))
  (ffi/read vidmode mode-ptr))

(defn get-window-pos
  "Returns an array @[xpos ypos]."
  [window]
  (def xpos-ptr (ffi/write :int 0))
  (def ypos-ptr (ffi/write :int 0))
  (glfwGetWindowPos window xpos-ptr ypos-ptr)
  (def xpos (ffi/read :int xpos-ptr))
  (def ypos (ffi/read :int ypos-ptr))
  @[xpos ypos])

(defn get-window-size
  "Returns an array @[width height]."
  [window]
  (def width-ptr (ffi/write :int 0))
  (def height-ptr (ffi/write :int 0))
  (glfwGetWindowSize window width-ptr height-ptr)
  (def width (ffi/read :int width-ptr))
  (def height (ffi/read :int height-ptr))
  @[width height])

(defn get-framebuffer-size
  "Returns an array @[width height]."
  [window]
  (def width-ptr (ffi/write :int 0))
  (def height-ptr (ffi/write :int 0))
  (glfwGetFramebufferSize window width-ptr height-ptr)
  (def width (ffi/read :int width-ptr))
  (def height (ffi/read :int height-ptr))
  @[width height])

(defn get-cursor-pos
  "Returns an array @[xpos ypos]."
  [window]
  (def xpos-ptr (ffi/write :double 0))
  (def ypos-ptr (ffi/write :double 0))
  (glfwGetCursorPos window xpos-ptr ypos-ptr)
  (def xpos (ffi/read :double xpos-ptr))
  (def ypos (ffi/read :double ypos-ptr))
  @[xpos ypos])
