(declare-project
  :name "glfw"
  :author "3gg@shellblade.net"
  :description "GLFW 3 bindings."
  :license "BSD-2"
  :url "https://gitlab.com/jeannekamikaze/janet-glfw.git")

(declare-source
  :source ["glfw.janet"])
