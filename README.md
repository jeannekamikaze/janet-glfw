# janet-glfw

Janet bindings for GLFW 3.

## Example

```janet
(import glfw)

(defn main [argv0]
  (if (not (glfw/init))
    (os/exit 1))
  (def window (glfw/create-window 640 480 "Hello World" nil nil))
  (if (not window)
    ((glfw/terminate)
    (os/exit 2)))
  (glfw/make-context-current window)
  (while (not (glfw/window-should-close window))
    (glfw/swap-buffers window)
    (glfw/poll-events))
  (glfw/terminate))
```

See `examples/` for more examples.

## Testing

Tested on Ubuntu 22.04 GNU/Linux, GLFW 3.3.6.

## Current Limitations

The bindings are currently missing support for:

- Callbacks (requires C glue)
- Joysticks
- Gamepads
- Various other bits of functionality.

## Contributing

Please add any missing functionality that is useful to you.

Please also add test code in `examples/02-example.janet`, where most of the
bindings are exercised.
