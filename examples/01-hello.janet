# A quasi-literal translation of https://www.glfw.org/documentation.html.
(import ../glfw)

(defn main [argv0]
  (if (not (glfw/init))
    (os/exit 1))
  (def window (glfw/create-window 640 480 "Hello World" nil nil))
  (if (not window)
    ((glfw/terminate)
    (os/exit 2)))
  (glfw/make-context-current window)
  (while (not (glfw/window-should-close window))
    (glfw/swap-buffers window)
    (glfw/poll-events))
  (glfw/terminate))
